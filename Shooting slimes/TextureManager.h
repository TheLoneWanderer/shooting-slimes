#pragma once
#include <SFML\Graphics.hpp>

#include "TmxManager.h"

#include <unordered_map>

// "Static" class used to manage sprite's textures
class TextureManager
{
public:
	TextureManager();
	~TextureManager();

	TextureManager(TextureManager& second);
	TextureManager& operator=(const TextureManager&) = delete;

	typedef std::unordered_map<std::string, sf::Texture> textureUnMap;

	typedef std::unordered_map<std::string, std::vector<sf::IntRect>> intRectVectUnMap;
	typedef std::unordered_map<std::string, intRectVectUnMap> textureFramesUnMap;

	struct AnimationInfo
	{
		explicit AnimationInfo() 
		{
		}

		AnimationInfo(TextureManager::textureUnMap::iterator tex, TextureManager::textureFramesUnMap::iterator states) :
			textureIter(tex),
			animationStatesIter(states)
		{
		}

		//const sf::Texture& textureRef;
		TextureManager::textureUnMap::iterator textureIter;
		TextureManager::textureFramesUnMap::iterator animationStatesIter;
	};

	// Textures naming convention: player0, player1, enemy0(...), where 0 means default
	TextureManager::AnimationInfo getTexture(std::string textureKeyName);

private:
	// When it hits 0, clear all textures
	static int textureManagerCount;

	static textureUnMap* textures;
	static textureFramesUnMap* textureFrames;

	TmxManager tmxMan;

	void init();
};