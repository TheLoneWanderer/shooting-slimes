#pragma once
#include "WeaponProperties.h"

#include "AudioManager.h"

#include <memory>

class EntityManager;

class Gun
{
public:
	Gun(WeaponProperties::GunProperties properties, EntityManager* entiManPtr);
	~Gun();
	
	const sf::Sprite& getSprite() const { return gunSprite; };
	void update();

	void shot(sf::Sprite& characterSprite, bool facingRight, WeaponProperties::ProjectileProperties projProps = WeaponProperties::ProjectileProperties(WeaponProperties::ProjectileProperties::projectileType::NORMAL, 0));

private:
	WeaponProperties::GunProperties gunProps;

	// Starting from Shooting animation texRect, not the whole spritesheet
	sf::Vector2f gunPositionDisplacement;

	// Starting from the gun's texRect position
	sf::Vector2f bulletStartPositionDisplacement;

	AudioManager audioMan;

	// Don't delete, it's only "watching" the object
	EntityManager* entiMan;

	sf::Texture* gunTexture;
	sf::Texture* bulletTexture;

	sf::Sprite gunSprite;

	std::vector<sf::IntRect>* gunTextureRects;

	// Shoot max. once per fireRateValue updates
	// eg. once per 4 updates
	int maxFireRate;

	// Can't shoot if lower than maxFireRate, incremented once per update()
	int weaponUseDelay;

	// Set to false after shot, changed back to true when ready
	bool readyToShoot;
};