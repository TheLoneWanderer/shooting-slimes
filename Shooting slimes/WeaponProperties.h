#pragma once
#include <SFML\Graphics.hpp>

class WeaponProperties
{
public:
	WeaponProperties();
	~WeaponProperties();

	struct GunProperties
	{
		enum class gunType { PISTOL, MACHINE_GUN };

		GunProperties(gunType type = gunType::PISTOL, int damage = 20, std::string gunOnwersTexName = "player0", std::string gunTexName = "pistol0", std::string bulletTexName = "pistolBullet0")
			: gunType(type), gunDamage(damage), gunOwnersTextureName(gunOnwersTexName), gunTextureName(gunTexName), bulletTextureName(bulletTexName)
		{
		}

		gunType gunType;
		int gunDamage;

		std::string gunOwnersTextureName;
		std::string gunTextureName;
		std::string bulletTextureName;
	};

	struct ProjectileProperties
	{
		enum class projectileType { NORMAL, EXPLOSIVE };

		ProjectileProperties(projectileType type = projectileType::NORMAL, int projDamage = 0, sf::Vector2f startVel = sf::Vector2f(0.0f, 0.0f))
			: projType(type), projectileDamage(projDamage), startingVelocity(startVel)
		{
		}

		projectileType projType;
		int projectileDamage;
		sf::Vector2f startingVelocity;
	};


};

