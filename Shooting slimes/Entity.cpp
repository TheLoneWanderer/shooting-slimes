#include "Entity.h"

#include <cstdlib>
#include <Windows.h>
#include <iostream>


Entity::Entity(sf::Texture& texRef, Entity::SpriteInfo& spriteInf)
{
	mainTexture = &texRef;
	mainSprite.setTexture(*mainTexture);
}


Entity::Entity(TextureManager::AnimationInfo& animInf, Entity::SpriteInfo& spriteInf) :
	animationInf(animInf)
{	
	using std::cout;
	using std::endl;

	mainTexture = &(animInf.textureIter->second);

	mainSprite.setTexture(*mainTexture);

	// Set the default sprite rect to idle, and keep the frames vector address (to decide which animation use in updateAnim())
	auto& defaultAnimFramesVect = animationInf.animationStatesIter->second.find("idle")->second;
	mainSprite.setTextureRect(defaultAnimFramesVect[0]);
	currentAnimFramesVect = &defaultAnimFramesVect;

	mainSprite.setPosition(spriteInf.position);
}

Entity::~Entity()
{
}

sf::Sprite& Entity::getSprite() const
{
	return const_cast<sf::Sprite&>(mainSprite);
}

sf::Vector2f& Entity::getVelocity()
{
	return velocity;
}

void Entity::setVelocity(float xVel, float yVel)
{
	velocity.x = xVel;
	velocity.y = yVel;
}

void Entity::move(float x, float y)
{
	mainSprite.move(x, y);
}

void Entity::reduceVelocity(const float maxVelX, const float maxVelY)
{
	if (fabs(velocity.x) > maxVelX)
	{
		if (velocity.x > 0.0f)
			velocity.x = maxVelX;
		else
			velocity.x = -maxVelX;
	}


	if (fabs(velocity.y) > maxVelY)
	{
		if (velocity.y > 0.0f)
			velocity.y = maxVelY;
		else
			velocity.y = -maxVelY;
	}
}

void Entity::draw(sf::RenderTarget& target, sf::RenderStates state) const
{
	target.draw(mainSprite);
}