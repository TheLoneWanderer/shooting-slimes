#pragma once
#include "Character.h"
#include "SFML\Graphics.hpp"

class Enemy : public Character
{
public:
	Enemy(TextureManager::AnimationInfo& animInf, Entity::SpriteInfo& spriteInf);
	~Enemy();

	float getMaxMoveRange() { return maxMoveRange; };

	virtual void update();

	virtual void onCollision(Entity* secondObject, CollisionProperties::collisionProperties& prop);
	virtual void onCollision(tmx::Object* secondObject, CollisionProperties::collisionProperties& prop);
private:
	// Used for detecting Player's position
	//float viewRange;

	// How far should the Enemy move from its spawn position
	// (used in "AI")
	float maxMoveRange = 80.0f;

	// -1: left, 0: stay, 1: right
	int moveDirection;

	// The actual and maximum values of the counter,
	// while it's lower than MAX, the enemy stays IDLE, then it starts moving
	int waitIdleCounter = 0;
	const int MAX_IDLE_DURATION = 60;

	// Animation delay (update animation once per max+1 update())
	const int ANIMATION_COUNTER_MAX = 3;

	sf::Vector2f spawnPointPos;

	virtual void draw(sf::RenderTarget& target, sf::RenderStates state) const;
};