#pragma once
#include <tmxlite\Map.hpp>
#include <tmxlite\MapLayer.hpp>

#include <unordered_map>
#include <memory>

class TmxManager
{
public:
	TmxManager();
	~TmxManager();

	TmxManager(TmxManager& second);
	TmxManager& operator=(const TmxManager&) = delete;

	// tmx::Map uses shared_ptr because of disabled copying ctor
	typedef std::unordered_map<std::string, std::shared_ptr<tmx::Map>> tmxMapsUnMap;

	// First unMap key should be the tmx file name, second would be the objGroup name
	// I.e.: first: "player0", second: "run" (for running animation cords)
	// This way it's easier to work with particular objectGroups (in case of tmx file containing more than one of them)
	typedef std::unordered_map<std::string, std::shared_ptr<tmx::ObjectGroup>> tmxObjGroupsUnMap;
	typedef std::unordered_map<std::string, tmxObjGroupsUnMap> tmxObjGroupsUnMapsUnMap;

	std::pair<bool, tmxMapsUnMap::iterator> getMap(std::string keyName);
	std::pair<bool, tmxObjGroupsUnMapsUnMap::iterator> getObjectGroups(std::string keyName);

private:
	// When it hits 0, clear all objects
	static int tmxManagerCount;

	static tmxMapsUnMap* tmxMaps;
	static tmxObjGroupsUnMapsUnMap* tmxObjGroups;

	void init();
};