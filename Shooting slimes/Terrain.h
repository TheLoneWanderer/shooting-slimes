#pragma once
#include <SFML\Graphics.hpp>

#include "AudioManager.h"
#include "TmxManager.h"

#include "Entity.h"

#include <memory>

class Terrain :public sf::Drawable, sf::Transformable
{
public:
	Terrain(std::string backgroundMapKey, std::string soundTrackKey);
	~Terrain();

	const sf::FloatRect& getCollisionLayerBounds() const { return foregroundLayer.getGlobalBounds(); };
	const tmx::ObjectGroup& getStaticObjects() const { return staticObjects; };
	const tmx::ObjectGroup& getEnemySpawnPoints() const { return enemySpawnPoints; };

private:
	AudioManager audioMan;
	TmxManager tmxMan;

	std::shared_ptr<sf::Music> currentLevelSoundtrack;

	tmx::MapLayer foregroundLayer;

	tmx::ObjectGroup staticObjects;
	tmx::ObjectGroup enemySpawnPoints;

	virtual void draw(sf::RenderTarget& target, sf::RenderStates state) const;
};