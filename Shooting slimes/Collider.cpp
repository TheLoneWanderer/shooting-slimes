#include "Collider.h"

#include "Game.h"
#include "Terrain.h"
#include "Projectile.h"


Collider::Collider(const Terrain& terrain) :
	terrain (terrain),
	qt(0, terrain.getCollisionLayerBounds())
{

}


Collider::~Collider()
{
}


void Collider::checkAllCollisions()
{
	// Empty the qt before inserting anything
	qt.clear();

	// Insert all objects to the quad tree:
	// The player
	qt.insert(entiMan.getPlayerAsEntity());
	
	// Dynamic objects
	for (auto entityIt : entiMan.getAllObjects())
		qt.insert(entityIt);

	// Static objects
	for (const auto& tmxObjIt : terrain.getStaticObjects().getObjects())
		qt.insert(&const_cast<tmx::Object&>(tmxObjIt));

	// Retrieve ready QTs and check collision
	auto& allSectors = qt.retrieve();
	checkCollision(allSectors);
}

void Collider::checkCollision(std::vector<QuadTree*>& qtRetrieveVect)
{
	// For each QuadTree
	for (int i = 0; i < qtRetrieveVect.size(); i++)
	{
		auto& currentQT = qtRetrieveVect[i];

		auto& dynamics = currentQT->getDynamics();
		auto& statics = currentQT->getStatics();

		// It's not the same as dynamics.size(), dynamics might be filled with NULLs,
		// which can lead to NULL dereference crash
		auto dynamicsAmount = currentQT->getDynamicsAmount();
		auto staticsAmount = currentQT->getStaticsAmount();

		// For each dynamic object
		for (int it = 0; it < dynamicsAmount; it++)
		{
			// Check for collision with other Dynamics
			// In the following manner: when there are i.e. 4 objects,
			// The first one checks all other,
			// The second only the remaining two, etc.
			//
			// This way we have 50% less collision checks,
			// provided that we modify the response part accordingly 
			// check the repond(Entity*, Entity*) method below for more details
	
			for (int iter = it+1; iter < dynamicsAmount; iter++)
			{
				Entity* first = dynamics[it];
				Entity* second = dynamics[iter];
				
				sf::FloatRect firstRect = first->getSprite().getGlobalBounds();
				sf::FloatRect secondRect = second->getSprite().getGlobalBounds();

				auto collisionProp = checkAABB(firstRect, secondRect);

				if (collisionProp.side.anySide)
				{
					respond(first, second, collisionProp);
				}
			}

			// Check for collision with Statics
			for (int iter = 0; iter < staticsAmount; iter++)
			{
				Entity* first = dynamics[it];
				tmx::Object* second = statics[iter];

				sf::FloatRect firstRect = first->getSprite().getGlobalBounds();

				sf::Vector2f secondPos (second->getPosition().x, second->getPosition().y);
				sf::Vector2f secondSize (second->getAABB().width, second->getAABB().height);

				sf::FloatRect secondRect(secondPos, secondSize);

				auto collisionProp = checkAABB(firstRect, secondRect);

				if (collisionProp.side.anySide)
				{
					respond(first, second, collisionProp);
				}
			}

		}
	}
}

CollisionProperties::collisionProperties Collider::checkAABB(sf::FloatRect& firstRect, sf::FloatRect& secondRect)
{
	using sf::Vector2f;
	using sf::Vector2u;

	auto move = [](sf::FloatRect& body, float dx, float dy)->void {body.left += dx; body.top += dy; };
	auto getHalfSize = [](sf::FloatRect& rect)->Vector2f {return Vector2f(rect.width / 2.0f, rect.height / 2.0f); };
	auto getPosition = [](sf::FloatRect& rect, sf::Vector2f halfSize)->Vector2f {return Vector2f(rect.left + halfSize.x, rect.top + halfSize.y); };
	
	Vector2f firstHalfSize = getHalfSize(firstRect);
	Vector2f secondHalfSize = getHalfSize(secondRect);

	Vector2f firstPosition = getPosition(firstRect, firstHalfSize);
	Vector2f secondPosition = getPosition(secondRect, secondHalfSize);

	float deltaX = secondPosition.x - firstPosition.x;
	float deltaY = secondPosition.y - firstPosition.y;
	float intersectX = abs(deltaX) - (secondHalfSize.x + firstHalfSize.x);
	float intersectY = abs(deltaY) - (secondHalfSize.y + firstHalfSize.y);

	CollisionProperties::collisionProperties collisionProp;
	collisionProp.intersect.x = intersectX;
	collisionProp.intersect.y = intersectY;

	if ((intersectX < 0.0f) && (intersectY < 0.0f))
	{
		if (intersectX > intersectY)
		{
			if (deltaX > 0.0f)
			{
				//std::cout << " Collision right " << std::endl;
				collisionProp.side.right = true;
			}

			else
			{
				//std::cout << " Collision left " << std::endl;
				collisionProp.side.left = true;
			}
		}

		else
		{
			if (deltaY > 0.0f)
			{
				//std::cout << " Collision bottom" << std::endl;
				collisionProp.side.bottom = true;
			}

			else
			{
				//std::cout << " Collision top" << std::endl;
				collisionProp.side.top = true;
			}
		}

		collisionProp.side.anySide = true;
	}

	return collisionProp;
}

void Collider::respond(Entity* first, Entity* second, CollisionProperties::collisionProperties& prop)
{
	first->onCollision(second, prop);
}

void Collider::respond(Entity* first, tmx::Object* second, CollisionProperties::collisionProperties& prop)
{
	first->onCollision(second, prop);
}
