#include "Projectile.h"

#include "EntityManager.h"

#include <cstdlib>
#include <ctime>


Projectile::Projectile(WeaponProperties::GunProperties& gunProps, WeaponProperties::ProjectileProperties projProps, sf::Texture& projTex, sf::Vector2f projPos)
	:Entity(projTex, Entity::SpriteInfo(projPos))
{
	entiType = Entity::entityType::PROJECTILE;

	setVelocity(projProps.startingVelocity.x, projProps.startingVelocity.y);
	getSprite().setPosition(projPos);
}


Projectile::~Projectile()
{
}


void Projectile::update()
{
	auto& velocity = getVelocity();

	move(velocity.x, velocity.y);
}

void Projectile::onCollision(Entity* secondObject, CollisionProperties::collisionProperties& prop)
{
	auto secondEntiType = secondObject->getEntityType();
	auto entiMan = getEntityManager();

	switch (secondEntiType)
	{
		case Entity::entityType::CHARACTER:
		{
			auto secondAsChar = static_cast<Character*>(secondObject);
			auto secondCharType = secondAsChar->getCharType();

			switch (secondCharType)
			{
				case Character::characterType::ENEMY:
				{
					// Play Sound
					audioMan.playSound("bulletHitChar0");

					// RemoveProjectile
					entiMan->removeEntity(this);

					// // For testing: destroy this enemy, change for recieving damage later
					entiMan->removeEntity(secondObject);

					// Deal damage
					// To be implemented
				} break;

				case Character::characterType::PLAYER:
				{
					// RemoveProjectile
					//entiMan->removeEntity(secondObject);

					// GetDamage
					// To be implemented
					// Should the player be damaged by its own projectiles?
				} break;
			}
		} break;
	}
}

void Projectile::onCollision(tmx::Object* secondObject, CollisionProperties::collisionProperties& prop)
{
	if (secondObject->getType() == "")
	{
		auto entiMan = getEntityManager();

		srand(time(0));
		std::string randomNumberStr = std::to_string(rand() % 5);

		audioMan.playSound("bulletHitWall" + randomNumberStr);

		entiMan->removeEntity(this);
	}
}