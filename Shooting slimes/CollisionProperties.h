#pragma once
#include <SFML\Graphics.hpp>

class CollisionProperties
{
public:
	CollisionProperties();
	~CollisionProperties();

	struct collisionProperties
	{
		struct collisionSide
		{
			bool anySide = false;

			bool left = false;
			bool right = false;
			bool top = false;
			bool bottom = false;
		} side;

		// How much should the objects be pushed away,
		// from each other in response()
		sf::Vector2f intersect;
	};
};

