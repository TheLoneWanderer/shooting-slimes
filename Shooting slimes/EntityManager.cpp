#include "EntityManager.h"
#include "Enemy.h"

//#include <iostream>
//
//using std::cout;
//using std::endl;

int EntityManager::entityManagerCount = 0;

Entity* EntityManager::playerObj = nullptr;

std::unordered_set<Entity*> EntityManager::entityObjects{ };

std::vector<Entity*> EntityManager::toBeDeletedBuffor{ };
std::vector<sf::Vector2f> EntityManager::spawnPoints{ };

EntityManager::EntityManager()
{
	entityManagerCount++;
}


EntityManager::~EntityManager()
{
	entityManagerCount--;

	if (entityManagerCount == 0)
	{
		// Delete all Entity objects...
		for (auto it : entityObjects)
		{
			delete it;
		}

		// ...and their pointers
		entityObjects.clear();

		// Delete the player
		delete playerObj;
	}
}

EntityManager::EntityManager(EntityManager& second)
{
	entityManagerCount++;
}

void EntityManager::spawnPlayer(sf::Vector2f position, std::string textureKeyName)
{
	auto& playerTexture = texMan.getTexture(std::move(textureKeyName));
	auto spriteProps = Entity::SpriteInfo(position);

	playerObj = new Player(playerTexture, spriteProps);
	playerObj->setEntityManager(this);
}

void EntityManager::spawnEnemy(sf::Vector2f position, std::string textureKeyName)
{
	auto& enemyTexture = texMan.getTexture(std::move(textureKeyName));
	auto spriteProps = Entity::SpriteInfo(position);

	auto enemy = new Enemy(enemyTexture, spriteProps);
	enemy->setEntityManager(this);

	entityObjects.insert(enemy);
}

void EntityManager::spawnEnemies(const tmx::ObjectGroup& spawnPoints)
{
	// Spawn one enemy at each spawn point
	for (auto& it : spawnPoints.getObjects())
	{
		auto& spawnPointPos = it.getAABB();

		spawnEnemy(sf::Vector2f(spawnPointPos.left, spawnPointPos.top));
	}
}

void EntityManager::addEntity(Entity* entityPtr)
{
	entityObjects.insert(entityPtr);
}

void EntityManager::removeEntity(Entity* entityPtr)
{
	toBeDeletedBuffor.emplace_back(entityPtr);
}


std::unordered_set<Entity*>& EntityManager::getAllObjects()
{
	for (int i = 0; i < toBeDeletedBuffor.size(); i++)
	{
		entityObjects.erase(toBeDeletedBuffor[i]);
	}

	toBeDeletedBuffor.clear();

	return entityObjects;
}

Entity* EntityManager::getPlayerAsEntity()
{
	return playerObj;
}

Player* EntityManager::getPlayer()
{
	return static_cast<Player*> (playerObj);
}

