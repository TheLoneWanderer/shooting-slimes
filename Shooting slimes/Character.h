#pragma once
#include "Entity.h"

class Character :public Entity
{
public:
	enum class characterType { PLAYER, ENEMY };
	enum class characterState { IDLE, RUN, JUMP, FALL, ON_LADDER, SHOOTING, DEAD };

	characterType getCharType() { return charType; };

	bool getIsOnGround() { return isOnGround; };
	void setIsOnGround(bool value) { isOnGround = value; };

protected:
	Character(TextureManager::AnimationInfo& animInf, Entity::SpriteInfo& spriteInf);
	~Character();

	characterType charType;

	characterState charState;
	characterState prevFrameCharState;
	
	bool isOnGround = false;

	// First one means the character is just standing on the ladder
	// Second means he's moving up/down the ladder
	bool climbingMode = false;
	bool currentlyClimbing = false;

	const float MOVEMENT_SPEED = 6.0f;
	const float JUMP_HEIGHT = 20.0f;
	const float GRAVITY_FORCE = 2.0f;

	// Used to slow down animation to fit movement speed
	// Redefine MAX in derived classes if necessary
	int animationCounter = 0;
	const int ANIMATION_COUNTER_MAX = 1;

	// Call inside the Entity derived update
	void updateAnimation(const int COUNTER_MAX_VAL);

	void stopObjectCollisionResponse(CollisionProperties::collisionProperties& prop);
};
