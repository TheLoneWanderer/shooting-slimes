#include "Menu.h"

#include "SFML\Graphics.hpp"

#include "Game.h"

#include <cstdlib>
#include <Windows.h>

Menu::Menu() :
	mState(menuState::IDLE),
	windowRef (Game::getWindow())
{
	menuBackgroundTex = texMan.getTexture("menuBackground0").textureIter->second;
	menuBackgroundSprite.setTexture(menuBackgroundTex);

	menuSoundTrack = audioMan.getMusic("menuBackgroundTrack0");
	titleFont = fontMan.getFont("piratesOfCydonia");
	gameOptionsFont = fontMan.getFont("peekingAssignment");

	menuSoundTrack->setLoop(true);
	menuSoundTrack->play();
}

Menu::~Menu()
{
	menuSoundTrack->stop();
}

void Menu::runMenuController()
{
	while (mState != menuState::EXIT)
	{
		switch (mState)
		{
			case menuState::IDLE:
			{
				idle();
			}
			break;

			case menuState::START_GAME:
			{
				startEngine();
			}
			break;

			case menuState::LOAD_SAVE:
			{
				loadSave();
			}
			break;

			case menuState::OPTIONS:
			{
				options();
			}
			break;
		}
	}

	if (Game::getGameState() != Game::gameState::ENGINE)
		Game::setGameState(Game::gameState::EXIT);
}

void Menu::idle()
{
	using std::string;

	using namespace sf;

	const int windowWidth = Game::getWindowWidth();
	const int windowHeight = Game::getWindowHeight();

	Text title("Shooting Slimes", titleFont, 55);

	// Setting up the title
	title.setOrigin(title.getGlobalBounds().width / 2, title.getGlobalBounds().height / 2);
	title.setPosition(windowWidth / 2, windowHeight / 12);
	title.setFillColor(Color(188, 102, 255, 255));

	// Setting up the game options
	const int menuOptionsAmount = 4;
	Text menuOptionsText[menuOptionsAmount];
	string menuOptionsStr[menuOptionsAmount]{ "New game", "Load game", "Options", "Exit"};

	for (int i = 0, verticalOffset = 80; i < menuOptionsAmount; i++)
	{
		menuOptionsText[i].setFont(gameOptionsFont);
		menuOptionsText[i].setString(menuOptionsStr[i]);
		menuOptionsText[i].setCharacterSize(45);

		menuOptionsText[i].setOrigin(menuOptionsText[i].getGlobalBounds().width / 2, menuOptionsText[i].getGlobalBounds().height / 2);
		menuOptionsText[i].setPosition(windowWidth / 2, windowHeight / 4 + i * verticalOffset);
	}

	// Main menu loop
	while (mState == menuState::IDLE)
	{
		Event eventM;
		Vector2f mousePos{ windowRef.mapPixelToCoords(Mouse::getPosition(windowRef)) };

		while (windowRef.pollEvent(eventM))
		{
			// Is the windows exit button pressed?
			if (eventM.type == Event::Closed)
				setMenuState(menuState::EXIT);

			// Is any of the buttons pressed?
			for (int i=0; i<menuOptionsAmount; i++)
				if (menuOptionsText[i].getGlobalBounds().contains(mousePos))
				{
					if (eventM.type == Event::MouseButtonReleased && eventM.key.code == Mouse::Left)
					{
						switch (i)
						{
						case 0:
							setMenuState(menuState::START_GAME);
							break;

						case 1:
							setMenuState(menuState::LOAD_SAVE);
							break;

						case 2:
							setMenuState(menuState::OPTIONS);
							break;

						case 3:
							setMenuState(menuState::EXIT);
							break;
						}
					}
				}
		}

		for (int i = 0; i < menuOptionsAmount; i++)
		{
			if (menuOptionsText[i].getGlobalBounds().contains(mousePos))
				menuOptionsText[i].setFillColor(Color::Cyan);
			else
				menuOptionsText[i].setFillColor(Color::White);

		}

		windowRef.clear();
		windowRef.draw(menuBackgroundSprite);
		windowRef.draw(title);

		for (int i = 0; i < menuOptionsAmount; i++)
		windowRef.draw(menuOptionsText[i]);

		windowRef.display();
	}
}

void Menu::startEngine()
{
	Menu::setMenuState(Menu::menuState::EXIT);
	Game::setGameState(Game::gameState::ENGINE);
}

void Menu::loadSave()
{
	using namespace sf;

	while (mState == (menuState::LOAD_SAVE))
	{
		Event eventM;
		Vector2f mousePos{ Mouse::getPosition(windowRef) };

		while (windowRef.pollEvent(eventM))
		{
			if (eventM.type == Event::Closed)
				setMenuState(menuState::EXIT);

			else if (eventM.type == Event::KeyReleased && eventM.key.code == Keyboard::Escape)
				setMenuState(menuState::IDLE);
		}

		windowRef.clear();
		windowRef.draw(menuBackgroundSprite);
		windowRef.display();
	}

	
}

void Menu::options()
{
	using namespace sf;

	while (mState == (menuState::OPTIONS))
	{
		Event eventM;
		Vector2f mousePos{ Mouse::getPosition(windowRef) };

		while (windowRef.pollEvent(eventM))
		{
			if (eventM.type == Event::Closed)
				setMenuState(menuState::EXIT);

			else if (eventM.type == Event::KeyReleased && eventM.key.code == Keyboard::Escape)
				setMenuState(menuState::IDLE);
		}

		windowRef.clear();
		windowRef.draw(menuBackgroundSprite);
		windowRef.display();
	}
	
}

void Menu::setMenuState(menuState state)
{
	mState = state;
}

