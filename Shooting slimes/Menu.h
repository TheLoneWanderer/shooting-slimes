#pragma once
#include <SFML\Graphics.hpp>

#include "AudioManager.h"
#include "FontManager.h"
#include "TextureManager.h"

#include <memory>

class Menu
{
public:
	Menu();
	~Menu();

	enum class menuState { IDLE, START_GAME, LOAD_SAVE, OPTIONS, EXIT };

	void runMenuController();
	void setMenuState(menuState state);

private:
	sf::RenderWindow& windowRef;
	menuState mState;

	AudioManager audioMan;
	FontManager fontMan;
	TextureManager texMan;

	std::shared_ptr<sf::Music> menuSoundTrack;
	sf::Font titleFont, gameOptionsFont;

	sf::Texture menuBackgroundTex;
	sf::Sprite menuBackgroundSprite;

	void idle();
	void startEngine();
	void loadSave();
	void options();
};