#pragma once
#include <SFML\Graphics.hpp>
#include "EntityManager.h"

#include "CollisionProperties.h"
#include "QuadTree.h"

class Terrain;
class Projectile;

class Collider
{
public:
	Collider(const Terrain& terrain);
	~Collider();
	
	// Call once per frame (engine update)
	void checkAllCollisions();

private:
	void checkCollision(std::vector<QuadTree*>& qtRetrieveVect);

	CollisionProperties::collisionProperties checkAABB(sf::FloatRect& firstRect, sf::FloatRect& secondRect);

	void respond(Entity* first, Entity* second, CollisionProperties::collisionProperties& prop);
	void respond(Entity* first, tmx::Object* second, CollisionProperties::collisionProperties& prop);

	const Terrain& terrain;
	EntityManager entiMan;
	QuadTree qt;

};

