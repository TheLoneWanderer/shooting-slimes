#pragma once
#include "Entity.h"
#include "Player.h"

#include "TextureManager.h"

#include "tmxlite\ObjectGroup.hpp"

#include <unordered_set>

// This class uses static storage for entity objects
// Each EntityManager uses the same container, when all instances of EM are gone, the container is emptied
//
// Entity objects are by default added in the engine (run-once code part)

class EntityManager
{
public:
	EntityManager();
	~EntityManager();

	EntityManager(EntityManager& second);
	EntityManager& operator=(const EntityManager&) = delete;

	void addEntity(Entity* entityPtr);
	void removeEntity(Entity* entityPtr);

	void spawnPlayer(sf::Vector2f position = sf::Vector2f(0, 0), std::string textureKeyName = "player0");


	void spawnEnemy(sf::Vector2f position = sf::Vector2f(0, 0), std::string textureKeyName = "enemy0");
	void spawnEnemies(const tmx::ObjectGroup& spawnPoints);

	std::unordered_set<Entity*>& getAllObjects();

	Entity* getPlayerAsEntity();
	Player* getPlayer();

private:
	TextureManager texMan;

	// When it hits 0, clear all Entity objects
	static int entityManagerCount;

	static Entity* playerObj;

	static std::unordered_set <Entity*> entityObjects;

	static std::vector<Entity*> toBeDeletedBuffor;
	static std::vector<sf::Vector2f> spawnPoints;
};

