#pragma once
#include "SFML\Graphics.hpp"

#include "Character.h"
#include "Gun.h"

class EntityManager;

class Player : public Character
{
public:
	Player(TextureManager::AnimationInfo& animInf, Entity::SpriteInfo& spriteInf);
	~Player();

	void setClimbingMode(bool value);
	
	virtual void update();

	virtual void onCollision(Entity* secondObject, CollisionProperties::collisionProperties& prop);
	virtual void onCollision(tmx::Object* secondObject, CollisionProperties::collisionProperties& prop);

private:
	Gun pistol;

	void clearCollisionFlags();
	virtual void draw(sf::RenderTarget& target, sf::RenderStates state) const;
};

