#include "Engine.h"

#include "Game.h"
#include "SFML\Graphics.hpp"

#include <Windows.h>
#include <iostream>

bool Engine::gameAlreadyRunning = false;

Engine::Engine():
	eState(engineState::PLAY),
	windowRef (Game::getWindow()),
	terrain("gameForeground0", "gameBackgroundTrack0"),
	collider(terrain)
{
	piratesOfCydonia = fontMan.getFont("piratesOfCydonia");
	peekingAssignment = fontMan.getFont("peekingAssignment");
}

Engine::~Engine()
{
	gameAlreadyRunning = false;
}

void Engine::runEngineController()
{
	while (eState != engineState::EXIT) 
	{
		switch (eState)
		{
			case engineState::PLAY:
			{
				play();
			}
			break;

			case engineState::PAUSE:
			{
				pause();
			}
			break;

			case engineState::PLAYER_DEAD:
			{
				playerDead();
			}
			break;
		}
	}
}

void Engine::play()
{
	using namespace sf;

	using std::cerr;
	using std::endl;

	// Place every "run-once" code here
	//========================================================
	if (!gameAlreadyRunning)
	{
		entiMan.spawnPlayer();
		entiMan.spawnEnemies(terrain.getEnemySpawnPoints());

		gameAlreadyRunning = true;
	}
	//========================================================

	Clock clock;
	View view(Vector2f(0.0f, 0.0f), Vector2f(Game::getWindowWidth(), Game::getWindowHeight()));

	double lag = 0.0;
	double previous = clock.getElapsedTime().asMilliseconds();

	while (eState == engineState::PLAY)
	{
		double current = clock.getElapsedTime().asMilliseconds();
		double elapsed = current - previous;
		previous = current;

		lag += elapsed;

		Event eventE;
		
		// Event loop
		if (windowRef.pollEvent(eventE))
		{
			if (eventE.type == Event::EventType::KeyReleased && eventE.key.code == Keyboard::Escape)
			{
				setEngineState(engineState::PAUSE);
			}	
		}

		// Updating stuff
		while (lag >= MS_PER_UPDATE)
		{
			updateLogic();
			lag -= MS_PER_UPDATE;

			// For testing
			//cerr << "Mouse Pos X = " << Mouse::getPosition(Game::getWindow()).x << endl << "Mouse Pos Y = " << Mouse::getPosition(Game::getWindow()).y << endl << endl;			
			//cerr << "Lag: " << lag << endl;
			//cerr << view.getCenter().x << " --- " << view.getCenter().y << endl;	
		}

		// Make the camera follow the player
			view.setCenter(entiMan.getPlayer()->getSprite().getPosition());
			windowRef.setView(view);

		// Rendering stuff
			drawScene();
		
	}
}

void Engine::pause()
{
	using namespace sf;
	using std::string;

	const int windowWidth = Game::getWindowWidth();
	const int windowHeight = Game::getWindowHeight();

	View view = windowRef.getView();

	// "PAUSED" settings
	Text pauseT ("PAUSED", peekingAssignment, 72);
	pauseT.setOrigin(pauseT.getLocalBounds().width / 2.0f, pauseT.getLocalBounds().height / 2.0f);
	pauseT.setPosition(view.getCenter().x, view.getCenter().y - windowHeight / 3.0f);

	pauseT.setStyle(Text::Italic);
	pauseT.setOutlineThickness(8.0);
	pauseT.setFillColor(Color::Magenta);

	// Paused options settings
	const int pauseOptionsAmount = 3;
	Text pauseOptionsText[pauseOptionsAmount];
	string pauseOptionsStr[pauseOptionsAmount]{ "CONTINUE", "BACK TO MENU", "EXIT GAME"};

	for (int i = 0; i < pauseOptionsAmount; i++)
	{
		pauseOptionsText[i].setCharacterSize(28);
		pauseOptionsText[i].setOutlineThickness(4.0);

		pauseOptionsText[i].setFont(piratesOfCydonia);
		pauseOptionsText[i].setString(pauseOptionsStr[i]);

		pauseOptionsText[i].setOrigin(pauseOptionsText[i].getLocalBounds().width / 2.0f, pauseOptionsText[i].getLocalBounds().height / 2.0f);
		pauseOptionsText[i].setPosition(view.getCenter().x, (view.getCenter().y - windowHeight / 6.0f) + i * 68);
	}
	
	
	// Pause event processing loop
	while (eState == engineState::PAUSE)
	{
		Event eventE;
		Vector2f mousePos{ windowRef.mapPixelToCoords(Mouse::getPosition(windowRef)) };
		

		if (windowRef.pollEvent(eventE))
		{
			for (int i = 0; i<pauseOptionsAmount; i++)
				if (pauseOptionsText[i].getGlobalBounds().contains(mousePos))
				{
					if (eventE.type == Event::MouseButtonReleased && eventE.key.code == Mouse::Left)
					{
						switch (i)
						{
						case 0:
						{
							setEngineState(engineState::PLAY);
						}
						break;

						case 1:
						{
							setEngineState(engineState::EXIT);
							Game::setGameState(Game::gameState::MENU);

							view.setCenter(windowWidth / 2.0f, windowHeight / 2.0f);
							windowRef.setView(view);
						}	
						break;

						case 2:
						{
							setEngineState(engineState::EXIT);
							Game::setGameState(Game::gameState::EXIT);
						}	
						break;
						}
					}
				}
		}

		for (int i = 0; i < pauseOptionsAmount; i++)
		{
			if (pauseOptionsText[i].getGlobalBounds().contains(mousePos))
				pauseOptionsText[i].setFillColor(Color::Cyan);
			else
				pauseOptionsText[i].setFillColor(Color::White);

		}


		windowRef.clear();
		windowRef.draw(terrain);
		windowRef.draw(pauseT);

		for (int i = 0; i < pauseOptionsAmount; i++)
		windowRef.draw(pauseOptionsText[i]);

		windowRef.display();
		
	}
}

void Engine::playerDead()
{
	
}

void Engine::updateLogic()
{
	collider.checkAllCollisions();

	entiMan.getPlayer()->update();
	
	for (auto entityObj : entiMan.getAllObjects())
	{
		entityObj->update();
	}
}

void Engine::drawScene(double sync)
{
	// Just rendering stuff
	windowRef.clear();

	windowRef.draw(terrain);
	windowRef.draw(*entiMan.getPlayer());

	for (auto entityObj : entiMan.getAllObjects())
		windowRef.draw(*entityObj);

	windowRef.display();
}

void Engine::setEngineState(engineState state)
{
	eState = state;
}
