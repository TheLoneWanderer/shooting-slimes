#include "Enemy.h"

#include "EntityManager.h"

#include <cstdlib>
#include <ctime>

//#include <iostream>

Enemy::Enemy(TextureManager::AnimationInfo& animInf, Entity::SpriteInfo& spriteInf)
	: Character (animInf, spriteInf)
{
	charType = characterType::ENEMY;

	spawnPointPos = spriteInf.position;

	srand(unsigned int (this));
	auto coinFlip = rand() % 2;

	if (coinFlip == 0)
		moveDirection = -1;
	else
		moveDirection = 1;
}

Enemy::~Enemy()
{
}

void Enemy::update()
{
	auto posX = getSprite().getPosition().x;
	auto posY = getSprite().getPosition().y;

	auto movementSpeed = MOVEMENT_SPEED / 12;
	auto gravityForce = GRAVITY_FORCE;

	auto& mainSprite = getSprite();
	auto& velocity = getVelocity();

	// Set previous state to use later in animation algorithm
	prevFrameCharState = charState;

	// Keep patroling the area in distance X (maxMoveRange) from the spawnpoint
	//
	// Example.: If the spawnpoint is at 40 (xPos) then if maxMoveRange is 10,
	// move between xPos 30 and 50
	
	int currentMoveDir = moveDirection;

	if (spawnPointPos.x + maxMoveRange < posX)
	{
		// Start moving back or just increment the counter
		if (waitIdleCounter > MAX_IDLE_DURATION)
		{
			currentMoveDir = moveDirection = -1;
			mainSprite.setPosition(sf::Vector2f(spawnPointPos.x + maxMoveRange, posY));

			charState = characterState::RUN;
			waitIdleCounter = 0;
		}

		else
		{
			velocity.x *= 0.50f;
			currentMoveDir = 0;
			waitIdleCounter++;
		}
	}
		

	else if (posX < spawnPointPos.x - maxMoveRange)
	{
		// Start moving back or just increment the counter
		if (waitIdleCounter > MAX_IDLE_DURATION)
		{
			currentMoveDir = moveDirection = 1;
			mainSprite.setPosition(sf::Vector2f(spawnPointPos.x - maxMoveRange, posY));

			charState = characterState::RUN;
			waitIdleCounter = 0;
		}

		else
		{
			velocity.x *= 0.50f;
			currentMoveDir = 0;
			waitIdleCounter++;
		}
	}

	switch (currentMoveDir)
	{
		// Move right
		case 1:
		{
			charState = characterState::RUN;
			velocity.x += movementSpeed;

			facingRight = true;
		}
		break;

		// Stay in place
		case 0:
		{
			// Maybe will do some stuff in the future
			charState = characterState::IDLE;
		}
		break;

		// Move left
		case -1:
		{
			charState = characterState::RUN;
			velocity.x -= movementSpeed;

			facingRight = false;
		} 
		break;	
	}

	if (!isOnGround)
	{
		velocity.y += gravityForce;
	}

	// Cut the velocity if it's too high to avoid tunneling
	// (passing through other objects)
	reduceVelocity(MAX_VELOCITY_X/4, MAX_VELOCITY_Y);

	mainSprite.move(velocity.x, velocity.y);

	updateAnimation(ANIMATION_COUNTER_MAX);
}

void Enemy::onCollision(Entity* secondObject, CollisionProperties::collisionProperties& prop)
{
	auto secondEntiType = secondObject->getEntityType();

	switch (secondEntiType)
	{
		case Entity::entityType::CHARACTER:
		{
			auto secondAsChar = static_cast<Character*>(secondObject);
			auto secondCharType = secondAsChar->getCharType();

			switch (secondCharType)
			{
				case Character::characterType::ENEMY:
				{
					// PushAway
					stopObjectCollisionResponse(prop);
				} break;

				case Character::characterType::PLAYER:
				{
					// Deal damage
					// To be implemented

					// PushAway
					stopObjectCollisionResponse(prop);
				} break;
			}
		} break;

		case Entity::entityType::PROJECTILE:
		{
			auto entiMan = getEntityManager();

			// Play Sound
			audioMan.playSound("bulletHitChar0");

			// RemoveProjectile
			entiMan->removeEntity(secondObject);

			// For testing: destroy this enemy, change for recieving damage later
			entiMan->removeEntity(this);

			// GetDamage
			// To be implemented
		} break;
	}
}

void Enemy::onCollision(tmx::Object* secondObject, CollisionProperties::collisionProperties& prop)
{
	auto& staticObjType = secondObject->getType();

	// If the object has any type (ex. ladder/door/trigger area),
	// repond based on the type
	if (staticObjType != "")
	{
		// Put any if (type == "stuff") responses below
	}

	// if not, do a regular response
	else
	{
		stopObjectCollisionResponse(prop);
	}
}

void Enemy::draw(sf::RenderTarget& target, sf::RenderStates state) const
{
	target.draw(getSprite());
}
