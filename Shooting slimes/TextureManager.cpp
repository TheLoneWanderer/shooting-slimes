#include "TextureManager.h"

#include <Windows.h>

// Initialize the statics
int TextureManager::textureManagerCount = 0;

TextureManager::textureUnMap* TextureManager::textures (nullptr);
TextureManager::textureFramesUnMap* TextureManager::textureFrames(nullptr);

TextureManager::TextureManager()
{
	textureManagerCount++;

	if (textureManagerCount == 1)
	{
		textures = new textureUnMap;
		textureFrames = new textureFramesUnMap;

		init();
	}
}

TextureManager::~TextureManager()
{
	textureManagerCount--;

	if (textureManagerCount == 0)
	{
		delete textures;
		delete textureFrames;
	}
}

TextureManager::TextureManager(TextureManager& second)
{
	textureManagerCount++;
}

TextureManager::AnimationInfo TextureManager::getTexture(std::string textureKeyName)
{
	// Return the texture and frames cords data, packed into one object

	auto& texRef = textures->find(textureKeyName);
	auto& texFrames = textureFrames->find(textureKeyName);

	return TextureManager::AnimationInfo(texRef, texFrames);
}


void TextureManager::init()
{
	// Add new textures here, and the frames below
	std::vector<std::pair<std::string, std::string>> texturesInfo
	{
		{ "menuBackground0", "Data/Graphics/backgrounds/menuBackground0/menuBackground0.png" },
		{ "player0", "Data/Graphics/characters/player/player0/player0.png" },
		{ "enemy0", "Data/Graphics/characters/enemy/enemy0/enemy0.png" },
		{ "pistol0", "Data/Graphics/weapons/pistols/pistol0/pistol0.png" },
		{ "pistolBullet0", "Data/Graphics/weapons/bullets/pistolBullet0/pistolBullet0.png" }
	};

	// Try to load texture to a temporary location and if failed, display error message
	for (int i = 0; i < texturesInfo.size(); i++)
	{
		sf::Texture currentTexture;

		// Load texture to the temporary, from the location listed above
		if (!currentTexture.loadFromFile(texturesInfo[i].second.c_str()))
		{
			std::string errorMessage = texturesInfo[i].second + " not found.";

			MessageBox(NULL, errorMessage.c_str(), ERROR, NULL);
			exit(0);
		}

		auto& textureKeyName = texturesInfo[i].first;

		// Add the key/Texture pair to the container
		textures->insert(std::make_pair(textureKeyName, currentTexture));
		//==============================================================================================================

		auto& currentTextureUnMap = tmxMan.getObjectGroups(texturesInfo[i].first);

		// Skip if there is no ObjGroup for this Texture
		if (!currentTextureUnMap.first)
			continue;

		else
		{

			// Add an empty "outer" unMap to the frames container, 
			// also keep a reference to the inserted pair
			auto currentOuterUnMapRef = textureFrames->insert(std::make_pair(textureKeyName, intRectVectUnMap()));

			for (auto& currentUnMapPair : currentTextureUnMap.second->second)
			{
				// Key of the inner ObjGroup, (i.e.: "run" for run animation inside outer "player0" group)
				const auto& unMapKey = currentUnMapPair.first;
				auto& tmxObjects = currentUnMapPair.second->getObjects();

				std::vector<sf::IntRect> tempIntRectVect;
				tempIntRectVect.reserve(tmxObjects.size());

				for (auto& currTmxObj : tmxObjects)
				{
					auto left = currTmxObj.getAABB().left;
					auto top = currTmxObj.getAABB().top;
					auto width = currTmxObj.getAABB().width;
					auto height = currTmxObj.getAABB().height;

					tempIntRectVect.emplace_back(sf::IntRect(left, top, width, height));
				}

				currentOuterUnMapRef.first->second.insert(std::make_pair(unMapKey, tempIntRectVect));
			}
		}
	}
}