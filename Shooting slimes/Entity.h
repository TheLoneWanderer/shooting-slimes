#pragma once
#include <SFML\Graphics.hpp>

#include "AudioManager.h"
#include "TextureManager.h"

#include "CollisionProperties.h"
#include <vector>

class EntityManager;

class Entity :public sf::Drawable, public sf::Transformable
{
public:
	virtual ~Entity();

	struct SpriteInfo
	{
		SpriteInfo(sf::Vector2f& pos) :
			position(pos) {}

		sf::Vector2f& position;
	};

	enum class entityType { CHARACTER, PROJECTILE };

	entityType getEntityType() { return entiType; };
	sf::Sprite& getSprite() const;
	sf::Vector2f& getVelocity();

	EntityManager* getEntityManager() { return entiMan; };
	void setEntityManager(EntityManager* entiManPtr) { entiMan = entiManPtr; };

	void setVelocity(float xVel, float yVel);
	void move(float x, float y);

	virtual void update() = 0;

	virtual void onCollision(Entity* secondObj, CollisionProperties::collisionProperties& prop) = 0;
	virtual void onCollision(tmx::Object* secondObj, CollisionProperties::collisionProperties& prop) = 0;

protected:
	Entity(sf::Texture& texRef, Entity::SpriteInfo& spriteInf = Entity::SpriteInfo(sf::Vector2f(0, 0)));
	Entity(TextureManager::AnimationInfo& animInf, Entity::SpriteInfo& spriteInf = Entity::SpriteInfo (sf::Vector2f (0,0)));

	// Used in collision detection
	entityType entiType;

	// Audio
	AudioManager audioMan;

	// Animations
	// Describes each state's X/Y and width/height (in pixels)
	TextureManager::AnimationInfo animationInf;
	std::vector<sf::IntRect>* currentAnimFramesVect;

	bool facingRight;

	// Current animation state/frame
	int animProgress;

	// Both should stay at (max) 20, because of possible tunneling/ghosting (AABB collision)
	const float MAX_VELOCITY_X = 20.0f;
	const float MAX_VELOCITY_Y = 20.0f;

	// Reduces velocity if above maximum value, use inside update()
	void reduceVelocity(const float maxVelX, const float maxVelY);

private:
	// Graphics
	sf::Texture* mainTexture;
	sf::Sprite mainSprite;

	// Movement
	sf::Vector2f velocity;

	// EntityManager
	EntityManager* entiMan;

	virtual void draw(sf::RenderTarget& target, sf::RenderStates state) const;
};