#include "FontManager.h"
#include <Windows.h>

#include <iostream>

using std::cout;
using std::endl;

// Initialize statics
int FontManager::fontManagerCount = 0;
FontManager::fontUnMap* FontManager::fonts (nullptr);

FontManager::FontManager()
{
	fontManagerCount++;

	if (fontManagerCount == 1)
	{
		fonts = new fontUnMap;
		init();
	}
}

FontManager::~FontManager()
{
	fontManagerCount--;

	if (fontManagerCount == 0)
	{
		delete fonts;
	}

}

FontManager::FontManager(FontManager& second)
{
	fontManagerCount++;
}

sf::Font& FontManager::getFont(std::string fontKeyName)
{
	// Return the font's reference found by its key
	return fonts->find(fontKeyName)->second;
}

void FontManager::init()
{
	std::vector <std::pair <std::string, std::string>> fontsInfo
	{
		{ "piratesOfCydonia", "Data/Fonts/piratesOfCydonia.otf" },
		{ "peekingAssignment", "Data/Fonts/peekingAssignment.ttf" }
	};

	// Try to load font to a temporary location and if failed, display error message
	for (int i = 0; i < fontsInfo.size(); i++)
	{
		sf::Font tempFont;

		// Load font to the temporary, from the location listed above
		if (!tempFont.loadFromFile(fontsInfo[i].second.c_str()))
		{
			std::string errorMessage = fontsInfo[i].second + " not found.";

			MessageBox(NULL, errorMessage.c_str(), ERROR, NULL);
			exit(0);
		}

		// Add the key/Font pair to the container
		fonts->insert(std::make_pair(fontsInfo[i].first, tempFont));
	}
}