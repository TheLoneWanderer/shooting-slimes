#pragma once
#include "Entity.h"

#include "WeaponProperties.h"

class Projectile : public Entity
{
public:
	Projectile(WeaponProperties::GunProperties& gunProps, WeaponProperties::ProjectileProperties projProps, sf::Texture& projTex, sf::Vector2f projPos);
	~Projectile();

	virtual void update();

	virtual void onCollision(Entity* secondObject, CollisionProperties::collisionProperties& prop);
	virtual void onCollision(tmx::Object* secondObject, CollisionProperties::collisionProperties& prop);

private:
	WeaponProperties::ProjectileProperties projProps;
};