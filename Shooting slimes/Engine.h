#pragma once
#include <SFML\Graphics.hpp>

#include "Terrain.h"
#include "Collider.h"

#include "FontManager.h"
#include "EntityManager.h"
#include "TextureManager.h"

class Engine
{
public:
	Engine();
	~Engine();

	enum class engineState { PLAY, PAUSE, PLAYER_DEAD, EXIT };

	void runEngineController();
	void setEngineState(engineState state);

private:
	const double MS_PER_UPDATE = 1000/30; // 1000/updatesPerSec

	// Used to determine whether to intialize objects (first run),
	// or just continue (i.e. user only paused the game and then pressed continue)
	static bool gameAlreadyRunning;

	engineState eState;

	sf::Font peekingAssignment, piratesOfCydonia;
	sf::RenderWindow& windowRef;

	Terrain terrain;
	FontManager fontMan;
	EntityManager entiMan;
	TextureManager texMan;
	Collider collider;

	void play();
	void pause();
	void playerDead();

	void updateLogic();
	void drawScene(double sync = 1.0);
};