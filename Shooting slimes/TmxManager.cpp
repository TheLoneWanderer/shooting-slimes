#include "TmxManager.h"

#include <tmxlite\detail\pugixml.hpp>

#include <Windows.h>
#include <cstdlib>

int TmxManager::tmxManagerCount = 0;
TmxManager::tmxMapsUnMap* TmxManager::tmxMaps (nullptr);
TmxManager::tmxObjGroupsUnMapsUnMap* TmxManager::tmxObjGroups (nullptr);

TmxManager::TmxManager()
{
	tmxManagerCount++;

	if (tmxManagerCount == 1)
	{
		tmxMaps = new tmxMapsUnMap;
		tmxObjGroups = new tmxObjGroupsUnMapsUnMap;

		init();
	}
}

TmxManager::~TmxManager()
{
	tmxManagerCount--;

	if (tmxManagerCount == 0)
	{
		delete tmxMaps;
		delete tmxObjGroups;
	}
}

TmxManager::TmxManager(TmxManager& second)
{
	tmxManagerCount++;
}

std::pair<bool, TmxManager::tmxMapsUnMap::iterator> TmxManager::getMap(std::string keyName)
{
	bool found;
	auto findResultIter = tmxMaps->find(keyName);

	if (findResultIter != tmxMaps->end())
		found = true;
	else
		found = false;

	return std::make_pair(found, findResultIter);
}

std::pair<bool, TmxManager::tmxObjGroupsUnMapsUnMap::iterator> TmxManager::getObjectGroups(std::string keyName)
{
	bool found;
	auto findResultIter = tmxObjGroups->find(keyName);

	if (findResultIter != tmxObjGroups->end())
		found = true;
	else
		found = false;

	return std::make_pair(found, findResultIter);
}

void TmxManager::init()
{
	std::vector<std::pair<std::string, std::string>> tmxFilesInfo
	{
		{ "gameForeground0", "Data/Graphics/backgrounds/gameForeground0/gameForeground0.tmx" },
		{ "player0", "Data/Graphics/characters/player/player0/player0.tmx" },
		{ "enemy0", "Data/Graphics/characters/enemy/enemy0/enemy0.tmx" },
		{ "pistol0", "Data/Graphics/weapons/pistols/pistol0/pistol0.tmx" },
		{ "bullet0", "Data/Graphics/weapons/bullets/pistolBullet0/pistolBullet0.tmx" }
	};

	for (int i = 0; i < tmxFilesInfo.size(); i++)
	{
		std::string& currentFileMapKey = tmxFilesInfo[i].first;
		std::string& currentFilePath = tmxFilesInfo[i].second;

		std::shared_ptr<tmx::Map> currentMapPtr = std::make_shared<tmx::Map>();

		// Load the tmx::Map from TMX file
		if (!currentMapPtr->load(currentFilePath.c_str()))
		{
			std::string errorMessage = "Error occured while trying to load " + currentFilePath;

			MessageBox(NULL, errorMessage.c_str(), ERROR, NULL);
			exit(0);
		}

		// ...and insert it into the container
		tmxMaps->insert(std::make_pair(currentFileMapKey, currentMapPtr));

		// Load the TMX file again, this time for parsing ObjectGroups out of it
		pugi::xml_document docXml;
		pugi::xml_parse_result parseResult = docXml.load_file(currentFilePath.c_str());

		if (!parseResult)
		{
			std::string errorMessage = "Error occured while trying to load " + currentFilePath;

			MessageBox(NULL, errorMessage.c_str(), ERROR, NULL);
			exit(0);
		}

		tmxObjGroupsUnMap tempUnMap;

		// Load each of the ObjectGroups to the temporary container
		for (auto objGroupNode = docXml.child("map").child("objectgroup"); objGroupNode; objGroupNode = objGroupNode.next_sibling("objectgroup"))
		{
			// Assemble the std::un_map key value in a following pattern: tmxMapKeyName_currentObjGroupName
			// Example: "gameForeground0_collisionLayer"
			std::string currentGroupName = objGroupNode.attribute("name").value();

			std::shared_ptr<tmx::ObjectGroup> currentGroupPtr = std::make_shared<tmx::ObjectGroup>(tmx::ObjectGroup());
			currentGroupPtr->parse(objGroupNode);

			tempUnMap.insert(std::make_pair(currentGroupName, currentGroupPtr));
		}

		tmxObjGroups->insert(std::make_pair(currentFileMapKey, tempUnMap));
	}
}