#include "QuadTree.h"

#include <stack>
#include <array>

std::vector<QuadTree*> QuadTree::allNodesInLine{};

QuadTree::QuadTree(int level, const sf::FloatRect& bounds)
{
	currentLevel = level;
	nodeSize = bounds;

	childNodes.reserve(4);
	childNodes.assign(4, nullptr);

	dynamicObjects.reserve(MAX_OBJECTS);
	staticObjects.reserve(MAX_OBJECTS);

	dynamicObjects.assign(MAX_OBJECTS, nullptr);
	staticObjects.assign(MAX_OBJECTS, nullptr);
}

QuadTree::~QuadTree()
{
	for (int i = 0; i < 4; i++)
		delete childNodes[i];
}

// Empties the QT nodes recursively, without deleting existing sub-nodes
// in order not to waste time for mem. allocation
void QuadTree::clear()
{
	// We don't need to delete them since it's EntityManager's responsibility,
	// also, just setting to nullptr is more efficient than clearing
	if (currentDynamicsAmount > 0)
	{
		//dynamicObjects.assign(currentDynamicsAmount, nullptr);
		currentDynamicsAmount = 0;
	}

	// Same reason as above (deleting)
	if (currentStaticsAmount > 0)
	{
		//staticObjects.assign(currentStaticsAmount, nullptr);
		currentStaticsAmount = 0;
	}

	// Clear other sub-nodes recursively
	// (do not mistake with vector.clear)
	if (childNodes[0] != nullptr)
	{
		for (int i = 0; i < 4; i++)
		{
			childNodes[i]->clear();
		}
	}

	allNodesInLine.clear();
}

// Splits the current node into 4 subnodes
void QuadTree::split()
{
	if (childNodes[0] == nullptr)
	{
	float subWidth = nodeSize.width / 2;
	float subHeight = nodeSize.height / 2;

	float x = nodeSize.left;
	float y = nodeSize.top;

	// Top left
		childNodes[0] = new QuadTree(currentLevel + 1, sf::FloatRect(x, y, subWidth, subHeight));
	// Top right
		childNodes[1] = new QuadTree(currentLevel + 1, sf::FloatRect(x + subWidth, y, subWidth, subHeight));
	// Bottom left
		childNodes[2] = new QuadTree(currentLevel + 1, sf::FloatRect(x, y + subHeight, subWidth, subHeight));
	// Bottom right
		childNodes[3] = new QuadTree(currentLevel + 1, sf::FloatRect(x + subWidth, y + subHeight, subWidth, subHeight));
	}
}

// Determine which node the object belongs to.
QuadTree::ObjectPlacement QuadTree::getIndex(const sf::FloatRect& objRect)
{
	ObjectPlacement objPlace { false, false, false, false, true };

	double verticalMidpoint = nodeSize.top + (nodeSize.height / 2);
	double horizontalMidpoint = nodeSize.left + (nodeSize.width / 2);

	// Object can completely fit within the top quadrants
	bool topQuadrant = (objRect.top < verticalMidpoint);
	// Same as above but in the bottom quadrants
	bool bottomQuadrant = (verticalMidpoint < (objRect.top + objRect.height));

	// Object can fit within the left quadrants
	if (objRect.left < horizontalMidpoint)
	{
		// Top left
		if (topQuadrant)
			objPlace.place[0] = true;

		// Bottom left
		if (bottomQuadrant)
			objPlace.place[2] = true;
	}

	// Same but within the right quadrants
	if (horizontalMidpoint < (objRect.left + objRect.width))
	{
		// Top right
		if (topQuadrant)
			objPlace.place[1] = true;

		// Bottom right
		if (bottomQuadrant)
			objPlace.place[3] = true;
	}
	
	for (int i = 0; i < 4; i++)
		if (!objPlace.place[i])
		{
			objPlace.fitsAll = false;
			break;
		}
			
	return objPlace;
}

void QuadTree::insert(Entity* entityObj)
{
	// If the object is empty, then don't add it
	if (entityObj == nullptr)
		return;

	sf::FloatRect objRect = entityObj->getSprite().getGlobalBounds();

	// If sub-nodes exist
	if (childNodes[0] != nullptr)
	{
		ObjectPlacement objPlace = getIndex(objRect);

		// Check recursively which nodes it fits into
		//if (!objPlace.fitsAll)
		//{
			for (int i = 0; i < 4; i++)
			{
				if (objPlace.place[i])
					childNodes[i]->insert(entityObj);
			}

			return;
		//}
	}

	// Check if the vector is full after inserting
	bool dynamicsVectIsFull = currentDynamicsAmount < MAX_OBJECTS ? false : true;
	bool maxLevelReached = currentLevel < MAX_LEVELS ? false : true;

	// If the objects contained in a node are about to equal MAX_OBJECTS size
	// Then try to split, if we didin't hit the sub-nodes limit already
	if (dynamicsVectIsFull && !maxLevelReached)
	{
		
		// Move dynamics from parent to child nodes, or leave them if they fit
		// Has to be first in order to split QT for statics
		reshuffleDynamics();

		// Same as above, but statics
		reshuffleStatics();

		// Insert the last element which couldn't fit into the parent node
		insert(entityObj);
		return;

	}

	// Vector not full / max_level doesn't matter
	if (!dynamicsVectIsFull)
	{
		dynamicObjects[currentDynamicsAmount] = entityObj;
		++currentDynamicsAmount;
	}

	// Vector full and max_level reached
	else
	{
		dynamicObjects.emplace_back(entityObj);
		++currentDynamicsAmount;
	}

}

void QuadTree::insert(tmx::Object* tmxObj)
{
	// If the object is empty then don't add it
	if (tmxObj == nullptr)
		return;

	const tmx::FloatRect& tmxTempRect = tmxObj->getAABB();
	sf::FloatRect objRect(tmxTempRect.left, tmxTempRect.top, tmxTempRect.width, tmxTempRect.height);

	// If sub-nodes exist
	if (childNodes[0] != nullptr)
	{
		ObjectPlacement objPlace = getIndex(objRect);

		for (int i = 0; i < 4; i++)
		{
			if (objPlace.place[i])
				childNodes[i]->insert(tmxObj);
		}

		return;
	}
	
	// Check if the vector is full after inserting
	bool staticsVectIsFull = currentStaticsAmount < MAX_OBJECTS ? false : true;
	bool maxLevelReached = currentLevel < MAX_LEVELS ? false : true;

	if (staticsVectIsFull && !maxLevelReached)
	{
		// Move statics from parent to child nodes, or leave them if they fit
		// Has to be first in order to split QT for dynamics
		reshuffleStatics();

		// Same as above, but dynamics
		reshuffleDynamics();

		// Insert the last element which couldn't fit into the parent node
		insert(tmxObj);
		return;
	}

	// Vector not full / max_level doesn't matter
	if (!staticsVectIsFull)
	{
		staticObjects[currentStaticsAmount] = tmxObj;
		++currentStaticsAmount;
	}

	// Vector full and max_level reached
	else 
	{
		staticObjects.emplace_back(tmxObj);
		++currentStaticsAmount;
	}
}

void QuadTree::reshuffleDynamics()
{
	if (childNodes[0] == nullptr)
		split();

	std::vector<Entity*> stayingInParentNode;
	stayingInParentNode.reserve(MAX_OBJECTS);

	// If a split occured, insert objects recursively into new nodes,
	// or leave them in the current one if they fit.
	for (int it = 0; it < currentDynamicsAmount; it++)
	{
		sf::FloatRect tempRect = dynamicObjects[it]->getSprite().getGlobalBounds();
		ObjectPlacement objPlace = getIndex(tempRect);

		// Check recursively which nodes it fits into
		for (int i = 0; i < 4; i++)
		{
			if (objPlace.place[i])
				childNodes[i]->insert(dynamicObjects[it]);
		}
	}

	// Clean the parent node
	//dynamicObjects.assign(currentDynamicsAmount, nullptr);
	currentDynamicsAmount = 0;
}

void QuadTree::reshuffleStatics()
{
	if (childNodes[0] == nullptr)
	{
		split();
	}

	// Insert objects recursively into new nodes,
	// or leave them in the current one if they fit.
	//
	// Keep here objects which should stay in parent node (not be moved into child nodes)
	std::vector<tmx::Object*> stayingInParentNode;
	stayingInParentNode.reserve(MAX_OBJECTS);

	for (int it = 0; it < currentStaticsAmount; it++)
	{
		tmx::Object& tmxTempObj = *staticObjects[it];
		const tmx::FloatRect& tmxTempRect = tmxTempObj.getAABB();

		sf::FloatRect objRect = sf::FloatRect(tmxTempRect.left, tmxTempRect.top, tmxTempRect.width, tmxTempRect.height);
		ObjectPlacement objPlace = getIndex(objRect);

		for (int i = 0; i < 4; i++)
		{
			if (objPlace.place[i])
				childNodes[i]->insert(&tmxTempObj);
		}
	}

	// Clean the parent node
	//staticObjects.assign(currentStaticsAmount, nullptr);
	currentStaticsAmount = 0;
}

std::vector<QuadTree*>& QuadTree::retrieve()
{
	// At first retrieve the result vector should be empty, so fill it
	if (allNodesInLine.empty())
	{
		allNodesInLine.emplace_back(this);

		// If any children nodes exist, add them recursively
		if (childNodes[0] != nullptr)
		{
			std::stack<QuadTree*> prevNodes;
			prevNodes.push(this);

			// While there are still nodes to search through
			while (!prevNodes.empty())
			{
				QuadTree* current = prevNodes.top();
				prevNodes.pop();

				// If child nodes exist, add all four to the stack
				if (current->childNodes[0] != nullptr)
				{
					for (int i = 0; i < 4; i++)
					{
						prevNodes.push(current->childNodes[i]);

						// If there are any dynamics inside, add to the result vector
						if (current->childNodes[i]->currentDynamicsAmount > 0)
							allNodesInLine.emplace_back(current->childNodes[i]);
					}
				}
			}
		}
	}	

	return allNodesInLine;
}