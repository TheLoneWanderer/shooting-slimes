#include "Game.h"

#include "Menu.h"
#include "Engine.h"

Game::gameState Game::gState = { gameState::MENU };
sf::RenderWindow Game::window{ sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "Shooting slimes", sf::Style::Resize };

Game::Game()
{
	window.setKeyRepeatEnabled(false);
	window.setVerticalSyncEnabled(true);
}

Game::~Game()
{
}

void Game::runProgramController()
{
	while (gState != gameState::EXIT)
	{
		switch (gState)
		{
			case gameState::MENU:
			{	
				Menu menu;
				menu.runMenuController();
			}
			break;

			case gameState::ENGINE:
			{
				Engine engine;
				engine.runEngineController();
			}
			break;
		}
		
	}
}

void Game::setGameState(Game::gameState state)
{
	gState = state;
}

sf::RenderWindow& Game::getWindow()
{
	return window;
}

const int Game::getWindowWidth()
{
	return WINDOW_WIDTH;
}

const int Game::getWindowHeight()
{
	return WINDOW_HEIGHT;
}

