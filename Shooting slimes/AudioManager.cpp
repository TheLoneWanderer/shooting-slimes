#include "AudioManager.h"

#include <Windows.h>

int AudioManager::audioManagerCount = 0;
std::shared_ptr<sf::Sound> AudioManager::currentlyPlayedSound = nullptr;

AudioManager::musicUnMap* AudioManager::musicTracksUnMap = nullptr;
AudioManager::soundsUnMap* AudioManager::gameSoundsUnMap = nullptr;

AudioManager::AudioManager()
{
	audioManagerCount++;

	if (audioManagerCount == 1)
	{
		currentlyPlayedSound = std::make_shared<sf::Sound>();

		musicTracksUnMap = new musicUnMap;
		gameSoundsUnMap = new soundsUnMap;

		init();
	}
}


AudioManager::~AudioManager()
{
	audioManagerCount--;

	if (audioManagerCount == 0)
	{
		delete musicTracksUnMap;
		delete gameSoundsUnMap;
	}
}

AudioManager::AudioManager(AudioManager& second)
{
	audioManagerCount++;
}

std::shared_ptr<sf::Music> AudioManager::getMusic(std::string musicKeyName)
{
	return musicTracksUnMap->find(musicKeyName)->second;
}

std::shared_ptr<sf::Sound> AudioManager::getSound(std::string soundKeyName)
{
	auto currentSoundBuffer = gameSoundsUnMap->find(soundKeyName)->second;

	std::shared_ptr<sf::Sound> currentSound = std::make_shared<sf::Sound>();
	currentSound->setBuffer(*currentSoundBuffer);

	return currentSound;
}

void AudioManager::playSound(std::string soundKeyName)
{
	auto currentSoundBuffer = gameSoundsUnMap->find(soundKeyName)->second;
	currentlyPlayedSound->setBuffer(*currentSoundBuffer);

	currentlyPlayedSound->play();
}

void AudioManager::init()
{
	std::vector<std::pair<std::string, std::string>> musicInfo
	{
		{ "menuBackgroundTrack0", "Data/Music/menuBackground/menuBackgroundTrack0.wav" },

		{ "gameBackgroundTrack0", "Data/Music/gameBackground/orangeFreeSounds/gameBackgroundTrack0.wav" }
	};

	std::vector<std::pair<std::string, std::string>> soundsInfo
	{
		{ "bulletHitChar0", "Data/Sounds/bulletHitCharacter/bulletHitCharacter0.wav" },

		{ "bulletHitWall0", "Data/Sounds/bulletHitWall/aust_paul/bulletHitWall0.wav" },
		{ "bulletHitWall1", "Data/Sounds/bulletHitWall/aust_paul/bulletHitWall1.wav" },
		{ "bulletHitWall2", "Data/Sounds/bulletHitWall/aust_paul/bulletHitWall2.wav" },
		{ "bulletHitWall3", "Data/Sounds/bulletHitWall/aust_paul/bulletHitWall3.wav" },
		{ "bulletHitWall4", "Data/Sounds/bulletHitWall/benboncan/bulletHitWall4.wav" },

		{ "pistolShot0", "Data/Sounds/pistolShot/pistolShot0.wav" }
	};

	for (int i = 0; i < musicInfo.size(); i++)
	{
		std::shared_ptr<sf::Music> currentTrack = std::make_shared<sf::Music>();

		if (!currentTrack->openFromFile(musicInfo[i].second))
		{
			std::string errorMessage = musicInfo[i].second + " not found.";

			MessageBox(NULL, errorMessage.c_str(), ERROR, NULL);
			exit(0);
		}

		auto& trackKeyName = musicInfo[i].first;
		musicTracksUnMap->insert(std::make_pair(trackKeyName, currentTrack));
	}

	for (int i = 0; i < soundsInfo.size(); i++)
	{
		std::shared_ptr<sf::SoundBuffer> currentSoundBuff = std::make_shared<sf::SoundBuffer>();

		if (!currentSoundBuff->loadFromFile(soundsInfo[i].second))
		{
			std::string errorMessage = soundsInfo[i].second + " not found.";

			MessageBox(NULL, errorMessage.c_str(), ERROR, NULL);
			exit(0);
		}

		auto& trackKeyName = soundsInfo[i].first;
		gameSoundsUnMap->insert(std::make_pair(trackKeyName, currentSoundBuff));

	}
}

