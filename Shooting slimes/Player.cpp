#include "Player.h"

#include "EntityManager.h"
#include "Game.h"
#include <iostream>


Player::Player(TextureManager::AnimationInfo& animInf, Entity::SpriteInfo& spriteInf):
	Character(animInf, spriteInf),
	pistol(WeaponProperties::GunProperties(), getEntityManager())
{
	charType = Character::characterType::PLAYER;
}


Player::~Player()
{
}

void Player::update()
{
	//-------------------------------------------
	// Main player method
	// Here we update velocity, get input etc.
	//-------------------------------------------

	using sf::RenderWindow;
	using sf::Keyboard;

	auto& mainSprite = getSprite();
	auto& velocity = getVelocity();

	// Slow the player down, so he stops more smoothly
	velocity.x *= 0.70f;
	
	// Keep track of previous state,
	// and set to IDLE if move button released and not falling/jumping
	prevFrameCharState = charState;

	if (isOnGround && charState != characterState::DEAD)
		charState = characterState::IDLE;

	//--------------------------------------------------------------------------------------
	// Movement
	//--------------------------------------------------------------------------------------

	// Move right
	if (Keyboard::isKeyPressed(Keyboard::D))
	{
		velocity.x += MOVEMENT_SPEED;

		if (charState == characterState::IDLE)
			charState = characterState::RUN;

		facingRight = true;
	}

	// Move left
	if (Keyboard::isKeyPressed(Keyboard::A))
	{
		velocity.x -= MOVEMENT_SPEED;

		if (charState == characterState::IDLE)
			charState = characterState::RUN;

		facingRight = false;
	}

	// Jump
	if (Keyboard::isKeyPressed(Keyboard::Space) && isOnGround)
	{
		velocity.y -= JUMP_HEIGHT;

		isOnGround = false;
		charState = characterState::JUMP;
	}
	
	if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left) && !climbingMode)
	{
		charState = characterState::SHOOTING;

		pistol.shot(getSprite(), facingRight);
	}

	// Climbing ladder
	if (climbingMode)
	{
		// Change player's state
		charState = characterState::ON_LADDER;

		if (Keyboard::isKeyPressed(Keyboard::W))
		{
			// Move up, set moving flag
			mainSprite.move(0.0f, -MOVEMENT_SPEED);
			currentlyClimbing = true;
		}

		else if (Keyboard::isKeyPressed(Keyboard::S))
		{
			// Move down, set moving flag
			mainSprite.move(0.0f, MOVEMENT_SPEED);
			currentlyClimbing = true;
		}

		else
		{
			currentlyClimbing = false;
		}
	}
	
	// Fall
	if (!isOnGround)
	{
		velocity.y += GRAVITY_FORCE;

		if (velocity.y > 0.0f)
			charState = characterState::FALL;
	}


	// Cut the velocity if it's too high to avoid tunneling
	// (passing through other objects)
	reduceVelocity(MAX_VELOCITY_X, MAX_VELOCITY_Y);	

	mainSprite.move(velocity.x, velocity.y);

	updateAnimation(ANIMATION_COUNTER_MAX);
	pistol.update();

	clearCollisionFlags();
}

void Player::setClimbingMode(bool value)
{
	climbingMode = value;
}

void Player::clearCollisionFlags()
{
	static bool previousClimbMode = false; 

	if (previousClimbMode && !climbingMode)
		isOnGround = false;

	previousClimbMode = climbingMode;
	climbingMode = false;
}

void Player::onCollision(Entity* secondObject, CollisionProperties::collisionProperties& prop)
{
	auto secondEntiType = secondObject->getEntityType();

	switch (secondEntiType)
	{
		case Entity::entityType::CHARACTER:
		{
			auto secondAsChar = static_cast<Character*>(secondObject);
			auto secondCharType = secondAsChar->getCharType();

			switch (secondCharType)
			{
				case Character::characterType::ENEMY:
				{
					// PushAway
					stopObjectCollisionResponse(prop);

					// GetDamage
					// To be implemented
				} break;

				case Character::characterType::PLAYER:
				{
					// PushAway
					stopObjectCollisionResponse(prop);
				} break;
			}
		} break;

		case Entity::entityType::PROJECTILE:
		{
			// RemoveProjectile
			//getEntityManager()->removeEntity(secondObject);

			// GetDamage
			// To be implemented
			// Should the player be damaged by its own projectiles?
		} break;
	}
}

void Player::onCollision(tmx::Object* secondObject, CollisionProperties::collisionProperties& prop)
{
	auto& staticObjType = secondObject->getType();

	// If the object has any type (ex. ladder/door/trigger area),
	// repond based on the type
	if (staticObjType != "")
	{
		if (staticObjType == "ladder")
		{
			this->setClimbingMode(true);
		}
	}

	// if not, do a regular response
	else
	{
		stopObjectCollisionResponse(prop);
	}
}

void Player::draw(sf::RenderTarget& target, sf::RenderStates state) const
{
	target.draw(getSprite());

	if (charState == characterState::SHOOTING)
	{
		target.draw(pistol.getSprite());
	}
}



