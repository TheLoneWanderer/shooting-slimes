#include "Terrain.h"

#include <tmxlite\detail\pugixml.hpp>

#include <cstdlib>
#include <Windows.h>
//#include <iostream>


Terrain::Terrain(std::string backgroundMapKey, std::string soundTrackKey)
{
	currentLevelSoundtrack = audioMan.getMusic(soundTrackKey);

	currentLevelSoundtrack->setLoop(true);
	currentLevelSoundtrack->play();

	auto& foregroundMapRef = tmxMan.getMap(backgroundMapKey);
	foregroundLayer.load(*foregroundMapRef.second->second, 0);

	auto& staticsRef = tmxMan.getObjectGroups(backgroundMapKey).second->second["collisionLayer"];
	auto& enemySpawnPointsRef = tmxMan.getObjectGroups(backgroundMapKey).second->second["enemySpawnPoints"];

	staticObjects = *staticsRef;
	enemySpawnPoints = *enemySpawnPointsRef;
}

Terrain::~Terrain()
{
	currentLevelSoundtrack->stop();
}

void Terrain::draw(sf::RenderTarget & target, sf::RenderStates state) const
{
	target.draw(foregroundLayer);
}


