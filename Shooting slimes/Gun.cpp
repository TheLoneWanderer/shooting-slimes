#include "Gun.h"

#include "Projectile.h"

#include "AudioManager.h"
#include "EntityManager.h"
#include "TextureManager.h"

Gun::Gun(WeaponProperties::GunProperties properties, EntityManager* entiManPtr)
	:gunProps (properties)
{
	entiMan = entiManPtr;

	TextureManager texMan;

	switch (gunProps.gunType)
	{
		case WeaponProperties::GunProperties::gunType::PISTOL:
		{
			maxFireRate = 8;
		} break;

		case WeaponProperties::GunProperties::gunType::MACHINE_GUN:
		{
			maxFireRate = 2;
		} break;
	}

	auto& gunOwnersTextureFiles = texMan.getTexture(properties.gunOwnersTextureName);
	auto& gunTextureFiles = texMan.getTexture(properties.gunTextureName);
	auto& bulletTextureFiles = texMan.getTexture(properties.bulletTextureName);

	// Load the gun's and bullet's texture
	gunTexture = &(gunTextureFiles.textureIter->second);
	bulletTexture = &(bulletTextureFiles.textureIter->second);

	// Calculate the gun's position displacement (how far from character's spriteRect's origin to final position)
	auto& shootingRectPos = gunOwnersTextureFiles.animationStatesIter->second.find("shooting")->second[0];
	auto& gunPosOnSpriteSheet = gunOwnersTextureFiles.animationStatesIter->second.find("shooting_gunPos")->second[0];

	gunPositionDisplacement = sf::Vector2f(gunPosOnSpriteSheet.left - shootingRectPos.left, gunPosOnSpriteSheet.top - shootingRectPos.top);
	
	// Same as above, but this time the bullet's position (from the gun's spriteRect)
	gunTextureRects = &(gunTextureFiles.animationStatesIter->second.find("fire")->second);
	auto& gunPosCordsOnSpriteSheet = (*gunTextureRects)[0];
	auto& bulletSpawnCordsOnSpriteSheet = gunTextureFiles.animationStatesIter->second.find("bulletSpawnPoint")->second[0];

	bulletStartPositionDisplacement = sf::Vector2f(bulletSpawnCordsOnSpriteSheet.left - gunPosCordsOnSpriteSheet.left, bulletSpawnCordsOnSpriteSheet.top - gunPosCordsOnSpriteSheet.top);

	gunSprite.setTexture(*gunTexture);
	gunSprite.setTextureRect(gunPosCordsOnSpriteSheet);
}

Gun::~Gun()
{
}

void Gun::update()
{
	weaponUseDelay++;

	if (weaponUseDelay > maxFireRate)
	{
		weaponUseDelay = 0;
		readyToShoot = true;
	}
}

void Gun::shot(sf::Sprite& characterSprite, bool facingRight, WeaponProperties::ProjectileProperties projProps)
{
	if (readyToShoot)
	{
		readyToShoot = false;

		auto& characterBounds = characterSprite.getGlobalBounds();
		auto& weaponTexRects = *gunTextureRects;

		// Both will be initialized in the following if statement depending on situation
		sf::Vector2f weaponPos;
		sf::Vector2f bulletPos;

		// Set weapon to face right
		if (facingRight)
		{
			weaponPos = sf::Vector2f(characterBounds.left + gunPositionDisplacement.x, characterBounds.top + gunPositionDisplacement.y);
			bulletPos = sf::Vector2f(weaponPos.x + bulletStartPositionDisplacement.x, weaponPos.y + bulletStartPositionDisplacement.y);

			gunSprite.setPosition(weaponPos);
			gunSprite.setTextureRect(weaponTexRects[0]);
			projProps.startingVelocity = sf::Vector2f(20.0f, 0.0f);
		}

		// Set weapon to face left
		else
		{
			auto& currentTexRect = weaponTexRects[0];
			auto weaponFlippedTexRect = sf::IntRect(currentTexRect.left + currentTexRect.width, currentTexRect.top, -abs(currentTexRect.width), currentTexRect.height);

			weaponPos = sf::Vector2f(characterBounds.left - currentTexRect.width - (gunPositionDisplacement.x - characterBounds.width), characterBounds.top + gunPositionDisplacement.y);
			bulletPos = sf::Vector2f(weaponPos.x - (bulletStartPositionDisplacement.x - currentTexRect.width), weaponPos.y + bulletStartPositionDisplacement.y);

			gunSprite.setPosition(weaponPos);
			gunSprite.setTextureRect(weaponFlippedTexRect);
			projProps.startingVelocity = sf::Vector2f(-20.0f, 0.0f);
		}

		auto projectile = new Projectile(gunProps, projProps, *bulletTexture, bulletPos);

		entiMan->addEntity(projectile);

		// Play Sound;
		audioMan.playSound("pistolShot0");
	}
}