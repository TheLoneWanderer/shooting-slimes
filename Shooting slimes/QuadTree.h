#pragma once
#include "Entity.h"
#include "SFML\Graphics.hpp"

#include <tmxlite\ObjectGroup.hpp>
#include <memory>
#include <utility>
#include <vector>

#include <iostream>


class QuadTree
{
public:
	QuadTree(int level, const sf::FloatRect& bounds);
	~QuadTree();

	void clear();
	void insert(Entity* entityObj);
	void insert(tmx::Object* tmxObj);

	sf::FloatRect& getSize() { return nodeSize; };
	
	int getLevel() { return currentLevel; };
	int getDynamicsAmount() { return currentDynamicsAmount; };
	int getStaticsAmount() { return currentStaticsAmount; };

	// Returns all QTs in a row (so we can iterate through them)
	std::vector<QuadTree*>& retrieve();

	// Used after retrieve(void)
	std::vector <Entity*>& getDynamics() { return dynamicObjects; };
	std::vector <tmx::Object*>& getStatics() { return staticObjects; };

private:
	struct ObjectPlacement
	{
		// 0 - topLeft, 1 - topRight
		// 2 - bottomLeft, 3 - bottomRight
		bool place[4];
		bool fitsAll;
	};

	// Must be at least size 1
	static const int MAX_OBJECTS = 10;
	static const int MAX_LEVELS = 5;
	
	// The "split-depth-level" of current node
	int currentLevel;

	// Objects contained within the node
	// 
	// We need to store the amount in another variable,
	// because the vector is reserved to be MAX_OBJECTS size
	// and filled with nullptrs, so we can't use vect.size() and emplace_back()
	int currentDynamicsAmount = 0;
	std::vector <Entity*> dynamicObjects;

	int currentStaticsAmount = 0;
	std::vector <tmx::Object*> staticObjects;

	// Size of the current node (in pixels)
	sf::FloatRect nodeSize;

	// Used to access the four child nodes
	std::vector<QuadTree*> childNodes;

	// All nodes in a row (each one without any children)
	static std::vector<QuadTree*> allNodesInLine;

	void split();

	// Called after split
	void reshuffleDynamics();
	void reshuffleStatics();

	ObjectPlacement getIndex(const sf::FloatRect& objRect);

};