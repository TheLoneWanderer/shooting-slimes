#pragma once
#include <SFML\Graphics.hpp>
#include <unordered_map>

class FontManager
{
public:
	FontManager();
	~FontManager();

	FontManager(FontManager& second);
	FontManager& operator=(const FontManager&) = delete;

	sf::Font& getFont(std::string fontKeyName);

private:
	// When it hits 0, clear all fonts
	static int fontManagerCount;

	typedef std::unordered_map<std::string, sf::Font> fontUnMap;
	static fontUnMap* fonts;

	void init();
};