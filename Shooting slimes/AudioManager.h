#pragma once
#include <SFML\Audio.hpp>

#include <unordered_map>
#include <memory>

class AudioManager
{
public:
	AudioManager();
	~AudioManager();

	AudioManager(AudioManager& second);
	AudioManager& operator=(AudioManager&) = delete;

	std::shared_ptr<sf::Music> getMusic(std::string musicKeyName);
	std::shared_ptr<sf::Sound> getSound(std::string soundKeyName);

	void playSound(std::string soundKeyName);

private:
	static int audioManagerCount;

	typedef std::unordered_map<std::string, std::shared_ptr<sf::Music>> musicUnMap;
	typedef std::unordered_map<std::string, std::shared_ptr<sf::SoundBuffer>> soundsUnMap;

	static std::shared_ptr<sf::Sound> currentlyPlayedSound;

	static musicUnMap* musicTracksUnMap;
	static soundsUnMap* gameSoundsUnMap;

	void init();
};

