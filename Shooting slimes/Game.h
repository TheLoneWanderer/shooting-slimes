#pragma once
#include <SFML\Graphics.hpp>

class Game
{
public:
	Game();
	~Game();


	enum class gameState { MENU, ENGINE, EXIT };

	void runProgramController();
	
	static sf::RenderWindow& getWindow();
	static const int getWindowWidth();
	static const int getWindowHeight();

	static void setGameState(gameState state);
	static gameState getGameState() { return gState; };

private:
	static sf::RenderWindow window;

	static const int WINDOW_WIDTH{ 1120 };
	static const int WINDOW_HEIGHT{ 736 };

	static gameState gState;
};