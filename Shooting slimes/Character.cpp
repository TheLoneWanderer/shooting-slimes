#include "Character.h"


Character::Character(TextureManager::AnimationInfo& animInf, Entity::SpriteInfo& spriteInf):
	Entity (animInf, spriteInf)
{
	entiType = Entity::entityType::CHARACTER;

	// Set the texture rect to default value
	currentAnimFramesVect = &animationInf.animationStatesIter->second["idle"];
	getSprite().setTextureRect((*currentAnimFramesVect)[0]);
}


Character::~Character()
{
}

void Character::updateAnimation(const int COUNTER_MAX_VAL)
{
	//--------------------------------------------------------------------------------------
	// Animations
	//--------------------------------------------------------------------------------------

	// Skip update if counter is below its max value,
	// reset counter and proceed to update otherwise
	if (COUNTER_MAX_VAL > animationCounter++)
	{
		return;
	}
		
	else
	{
		animationCounter = 0;
	}


	switch (charState)
	{
		case characterState::RUN:
		{
			currentAnimFramesVect = &animationInf.animationStatesIter->second["run"];
		
		} break;

		case characterState::IDLE:
		{
			currentAnimFramesVect = &animationInf.animationStatesIter->second["idle"];
		
		} break;

		case characterState::JUMP:
		{
			currentAnimFramesVect = &animationInf.animationStatesIter->second["jump"];
		
		} break;

		case characterState::FALL:
		{
			currentAnimFramesVect = &animationInf.animationStatesIter->second["fall"];
		
		} break;

		case characterState::DEAD:
		{
			currentAnimFramesVect = &animationInf.animationStatesIter->second["dead"];
		
		} break;

		case characterState::ON_LADDER:
		{
			currentAnimFramesVect = &animationInf.animationStatesIter->second["climbing"];
		
		} break;

		case characterState::SHOOTING:
		{
			currentAnimFramesVect = &animationInf.animationStatesIter->second["shooting"];
		
		} break;
	}
		

	// If player state changed (ie. from idle to run) or animation ended -> reset animation
	if (prevFrameCharState != charState || animProgress >= currentAnimFramesVect->size())
		animProgress = 0;

	auto flipRectLeft = [](sf::IntRect& rect)->sf::IntRect { return sf::IntRect(rect.left + rect.width, rect.top, -abs(rect.width), rect.height); };
	auto setTexRect = [&flipRectLeft](sf::Sprite& spriteRef, sf::IntRect& nextFrameRect, bool facingRight)->void
	{
		if (facingRight)
			spriteRef.setTextureRect(nextFrameRect);
		else
			spriteRef.setTextureRect(flipRectLeft(nextFrameRect));
	};

	// Set textureRect based on animation progress and facingRight
	auto& nextTextureState = (*currentAnimFramesVect)[animProgress];
	setTexRect(getSprite(), nextTextureState, facingRight);

	// If the player is climbing ladder then update animProgress only if moving, otherwise just update
	if (charState == characterState::ON_LADDER)
	{
		if (currentlyClimbing)
			animProgress++;
	}
	else
		animProgress++;
		
}

void Character::stopObjectCollisionResponse(CollisionProperties::collisionProperties& prop)
{
	this->setVelocity(0.0f, 0.0f);

	if (prop.side.bottom)
	{
		this->move(0.0f, prop.intersect.y);
		this->setIsOnGround(true);
	}

	else if (prop.side.top)
	{
		this->move(0.0f, -prop.intersect.y);
	}

	if (prop.side.right)
	{
		this->move(prop.intersect.x, 0.0f);
	}

	else if (prop.side.left)
	{
		this->move(-prop.intersect.x, 0.0f);
	}
}